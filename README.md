#BlueAlly Coding Challenge Project Setup
Symfony 4 API, Angular 7 frontend

---
## NOT FOR PRODUCTION
Includes cors hack to allow running without changing of hosts files.
This project is NOT production-safe.

---

## Structure

        Database:
        - MySql
        - PhpMyAdmin
        
        Server:
        - PHP
        - Apache
        
        Frond End:
        - NGINX
        - Node

## Setup
Build project
```bash
$ docker-compose build
```
Start server
```bash
$ docker-compose up
```

## Install Fixtures Data
Get PHP container image
```bash
$ docker ps
```
Note 'blueallychallenge_php' container and copy the container name and SSH into that container
```bash
$ docker exec -it {container_name} bash
```
After you've successfully SSH'd in to the container, cd to the project directory
```bash
$ cd sf4
```

Once in the sf4 directory, run the following
```bash
$ php bin\console doctrine:migrations:migrate
```
Then install the dummy fixture data
```bash
$ php bin\console doctrine:fixutres:load
```

## Useful
If errors occur with Symfony 4, run
```bash
$ composer install
```
from the /apps/api directory

---
If errors occur with Angular 7, run
```bash
npm install
```
from the /apps/client directory


