<?php

namespace App\Repository;

use App\Entity\Ip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ip|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ip|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ip[]    findAll()
 * @method Ip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IpRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ip::class);
    }

    public function transform(Ip $ip)
    {
        return [
            'id' => (int) $ip->getId(),
            'address' => (string) $ip->getAddress(),
            'address_tag' => (string) $ip->getAddressTag(),
            'subnet' => (string) $ip->getSubnet()->getAddress()
        ];
    }

    public function transformAll()
    {
        $ips = $this->findAll();
        $ipsArray = [];

        foreach ($ips as $ip) {
            $ipsArray[] = $this->transform($ip);
        }

        return $ipsArray;
    }

    // /**
    //  * @return Ip[] Returns an array of Ip objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ip
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
