<?php

namespace App\Repository;

use App\Entity\Subnet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Subnet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Subnet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Subnet[]    findAll()
 * @method Subnet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubnetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Subnet::class);
    }

    public function transform(Subnet $subnet)
    {
        $ipArray = [];

        foreach($subnet->getIps() as $subnetIp) {
            $ipArray[] = [
                "id" => (int) $subnetIp->getId(),
                "address" => (string) $subnetIp->getAddress(),
                "address_tag" => (string) $subnetIp->getAddressTag()
            ];
        }

        return [
            'id' => (int) $subnet->getId(),
            'address' => (string) $subnet->getAddress(),
            'cidr' => (string) $subnet->getCidr(),
            'ips' => $ipArray
        ];
    }

    public function transformAll()
    {
        $subnets = $this->findAll();
        $subnetsArray = [];

        foreach ($subnets as $subnet) {
            $subnetsArray[] = $this->transform($subnet);
        }

        return $subnetsArray;
    }

    // /**
    //  * @return Subnet[] Returns an array of Subnet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Subnet
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
