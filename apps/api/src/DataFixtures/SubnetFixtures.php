<?php

namespace App\DataFixtures;

use App\Entity\Subnet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SubnetFixtures extends Fixture
{
    /**
     * @var array
     */
    protected $subnetStub = [
        ["address" => "198.178.91.0", "cidr" => "28"],
        ["address" => "198.178.92.0", "cidr" => "30"],
        ["address" => "198.178.93.0", "cidr" => "29"]
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $x = 1;

        foreach ($this->subnetStub as $subnetData) {
            $subnet = new Subnet();
            $subnet->setAddress($subnetData["address"]);
            $subnet->setCidr($subnetData["cidr"]);

            $ref_tag = 'subnet_' . $x;
            echo $ref_tag;
            $this->addReference($ref_tag, $subnet);
            $manager->persist($subnet);

            $x = $x + 1;
        }

        $manager->flush();
    }
}
