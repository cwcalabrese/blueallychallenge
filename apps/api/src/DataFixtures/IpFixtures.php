<?php

namespace App\DataFixtures;

use App\Entity\Ip;
use App\DataFixtures\SubnetFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class IpFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var array
     */
    protected $ipStub = [
        ["address" => "198.178.91.0", "address_tag" => "<eq_03_xnbwb_4_mgmt_ip>", "subnet" => 1],
        ["address" => "198.178.91.1", "address_tag" => "<opz_xd_5_primary_uplink_netwk_ip>", "subnet" => 1],
        ["address" => "198.178.91.2", "address_tag" => "<opz_xd_5_primary_uplink_broadcast_ip>", "subnet" => 1],
        ["address" => "198.178.91.3", "address_tag" => "<opz_xd_5_secondary_uplink_netwk_ip>", "subnet" => 1],
        ["address" => "198.178.91.4", "address_tag" => "<zb500_opz_xd_5_secondary_uplink_ip>", "subnet" => 1],
        ["address" => "198.178.91.5", "address_tag" => "<opz_xd_02_opz_xd_5_secondary_uplink_ip>", "subnet" => 1],
        ["address" => "198.178.91.6", "address_tag" => "<opz_xd_5_secondary_uplink_broadcast_ip>", "subnet" => 1],
        ["address" => "198.178.91.7", "address_tag" => "<opz_telemetry_broadcast_ip>", "subnet" => 1],
        ["address" => "198.178.91.8", "address_tag" => "<opz_opz_oam_uplink_vip_ip>", "subnet" => 1],
        ["address" => "198.178.91.9", "address_tag" => "<zb500_02_opz_oam_uplink_ip>", "subnet" => 1],
        ["address" => "198.178.91.10", "address_tag" => "<zb500_01_opz_oam_uplink_ip>", "subnet" => 1],
        ["address" => "198.178.91.11", "address_tag" => "<zb500_opz_oam_uplink_vip_ip>", "subnet" => 1],
        ["address" => "198.178.91.12", "address_tag" => "<opz_telemetry_netwk_ip>", "subnet" => 1],
        ["address" => "198.178.91.13", "address_tag" => "<opz_telemetry_broadcast_ip>", "subnet" => 1],
        ["address" => "198.178.91.14", "address_tag" => "<opz_telemetry_default_gateway_ip>", "subnet" => 1],
        ["address" => "198.178.91.15", "address_tag" => "<eq_03_mgmt_ip>", "subnet" => 1],

        ["address" => "198.178.92.0", "address_tag" => "<zb500_primary_opz_untrust_ibgp_ip>", "subnet" => 2],
        ["address" => "198.178.92.1", "address_tag" => "<opz_untrust_ibgp_netwk_ip>", "subnet" => 2],
        ["address" => "198.178.92.2", "address_tag" => "<zb500_secondary_opz_untrust_ibgp_ip>", "subnet" => 2],
        ["address" => "198.178.92.3", "address_tag" => "<opz_untrust_ibgp_broadcast_ip>", "subnet" => 2],

        ["address" => "198.178.93.0", "address_tag" => "<zb500_02_mgmt_re1_ip>", "subnet" => 3],
        ["address" => "198.178.93.1", "address_tag" => "<zb500_02_mgmt_re0_ip>", "subnet" => 3],
        ["address" => "198.178.93.2", "address_tag" => "<zb500_02_mgmt_vip_ip>", "subnet" => 3],
        ["address" => "198.178.93.3", "address_tag" => "<zb500_01_mgmt_re1_ip>", "subnet" => 3],
        ["address" => "198.178.93.4", "address_tag" => "<zb500_01_mgmt_re0_ip>", "subnet" => 3],
        ["address" => "198.178.93.5", "address_tag" => "<zb500_01_mgmt_vip_ip>", "subnet" => 3],
        ["address" => "198.178.93.6", "address_tag" => "<opz_telemetry_netwk_ip>", "subnet" => 3],
        ["address" => "198.178.93.7", "address_tag" => "<xnbwb_5_mgmt_broadcast_ip>", "subnet" => 3]
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->ipStub as $ipData) {
            $ip = new Ip();
            $ip->setAddress($ipData["address"]);
            $ip->setAddressTag($ipData["address_tag"]);
            $ref_tag = 'subnet_' . $ipData["subnet"];
            echo $ref_tag;
            $ip->setSubnet($this->getReference($ref_tag));
            $manager->persist($ip);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            SubnetFixtures::class
        );
    }
}
